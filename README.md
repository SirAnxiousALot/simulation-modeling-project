# Made for Simulation Project

- Imitates the Monte Carlo Simulation.
- Made using HTML/JS & Material Design.
- Not meant to be public.

## Team Members (IDs)
- 20160241 (Leader)
- 20160332 (Member)
- 20180334 (Member)
