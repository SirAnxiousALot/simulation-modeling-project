let count = 1;

const reloadPage = () => {
  let elm = document.getElementById("simulationsCount");
  elm.value = 1;
  location.reload();
};

const addRow = () => {
  let newDiv = document.createElement("tr");
  newDiv.setAttribute("id", `tr${count}`);
  newDiv.setAttribute("class", `trInput`);
  newDiv.innerHTML = `
  <td><input class="inputa" id="ida${count}" type="number" value="0"></td>
  <td><input class="inputf" id="idf${count}" type="number" value="0"></td>
  <td><button id="delete${count}" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect" onclick="removeRow(${count})">❌</button>
  </td>`;
  let div2use = document.getElementById("rowz");
  div2use.appendChild(newDiv);
  console.log(newDiv);
  count = count + 1;
};

const removeRow = countId => {
  let elm = document.getElementById(`tr${countId}`);
  elm.remove();
};

const getTotal = value => {
  let resTotal = 0.0;
  let elm;
  value == 0
    ? (elm = document.getElementsByClassName("inputf"))
    : (elm = document.getElementsByClassName("inputa"));
  for (let i = 0; i < elm.length; i++) {
    if (typeof Number(elm[i].value) === "number" && Number(elm[i].value) > 0) {
      resTotal += Number(elm[i].value);
    } else {
      elm[i].value = "0";
    }
  }
  return resTotal;
};

const cleanDiv = div => {
  let div2use = document.getElementById(div);
  let child1 = div2use.lastElementChild;
  let child2 = div2use.firstElementChild;
  while (child1 != child2) {
    div2use.removeChild(child1);
    child1 = div2use.lastElementChild;
  }
};

const findResult = () => {
  // Get total Freq & total Amount
  let totalFreq = getTotal(0);
  let totalAmount = getTotal(1);
  let cProbability = 0.0;
  let numberRange = 0;
  let numberRanges = [];
  let totalSimulated = 0;
  let expectedResult = 0;
  let expectedString = "";

  let div2use = document.getElementById("rrow");

  cleanDiv("rrrow");
  cleanDiv("rrow");

  let elm = document.getElementsByClassName("trInput");
  for (let i = 0; i < elm.length; i++) {
    let id = Number(elm[i].id.toString().replace("tr", ""));
    let newRow = document.createElement("tr"); // Amount - Freq - Probability - cProbability - Number Range
    if (document.getElementById(`idf${id}`).value / totalFreq != 0) {
      numberRanges.push([
        Number(numberRange) + 1,
        Number(
          (numberRange =
            numberRange +
            (document.getElementById(`idf${id}`).value / totalFreq) * 100)
        )
      ]);
    } else numberRanges.push([numberRange, numberRange]);
    newRow.innerHTML = `
    <td>${document.getElementById(`ida${id}`).value}</td>
    <td>${document.getElementById(`idf${id}`).value}</td>
    <td>${document.getElementById(`idf${id}`).value / totalFreq}</td>
    <td>${(cProbability =
      cProbability +
      document.getElementById(`idf${id}`).value / totalFreq)}</td>
    <td>${numberRanges[i][0]} - ${numberRanges[i][1]}</td>
    `;
    expectedResult +=
      (document.getElementById(`ida${id}`).value *
        document.getElementById(`idf${id}`).value) /
      totalFreq;
    expectedString += `(${
      document.getElementById(`ida${id}`).value
    })(${document.getElementById(`idf${id}`).value / totalFreq}) `;
    if (i != elm.length - 1) {
      expectedString += "+ ";
    }
    div2use.appendChild(newRow);
  }

  div2use = document.getElementById("rrrow");
  let simCount = document.getElementById("simulationsCount");
  for (let i = 0; i < simCount.value; i++) {
    let newData = document.createElement("tr"); // Simulation # =, Random Number, Simulated Result
    let randomNum = Math.floor(Math.random() * 100) + 1;
    let elm = document.getElementsByClassName("trInput");
    let simulatedResult = 0;

    for (let i = 0; i < elm.length; i++) {
      let id = Number(elm[i].id.toString().replace("tr", ""));
      if (randomNum >= numberRanges[i][0] && randomNum <= numberRanges[i][1]) {
        simulatedResult = Number(document.getElementById(`ida${id}`).value);
        totalSimulated += simulatedResult;
      }
    }
    newData.innerHTML = `
    <td>${i + 1}</td>
    <td>${randomNum}</td>
    <td>${simulatedResult}</td>
    `;
    div2use.appendChild(newData);
  }
  resultDiv = document.getElementById("resultText");
  resultDiv.innerHTML = `<center>
  Average Count during ${simCount.value} simulation = ${totalSimulated /
    simCount.value}
  <br/>
  ${expectedString} = ${expectedResult}
    </center>
  `;
};
